#!/bin/bash
N=1
if [ $# -eq 1 ]
    then
        echo "Repeating: $1 times"
        N=$1 
fi
echo $N 

for i in $(seq 1 $N)
do
     echo $i
     ./bin/scanConsole -r configs/controller/specCfg-rd53a.json -c configs/connectivity/example_rd53a_setup.json -s configs/scans/rd53a/std_digitalscan.json -p 
done
