#!/usr/bin/python
import sys
import time
from plx_gpib_ethernet import PrologixGPIBEthernet
class agilent:
    def __init__(self,ip,addr):
        self.gpib= PrologixGPIBEthernet(ip)
        self.gpib.connect()
        self.gpib.select(addr)
    def __exit__(self):
        self.gpib.close()
    def read(self,output):
        self.gpib.write("INST:SEL OUT"+str(output))
        volt=float(self.gpib.query('MEAS:VOLT?').rstrip('\n'))
        curr=float(self.gpib.query('MEAS:CURR?').rstrip('\n'))
        return volt,curr
    def on(self,output):
        self.gpib.write("INST:SEL OUT"+str(output))
        self.gpib.write("VOLT 1.80")
    def off(self,output):
        self.gpib.write("INST:SEL OUT"+str(output))
        self.gpib.write("VOLT 0.0")
        
def usage():
    print "power {stat|off|on|cycle} {1|2}"
    print "      command=stat|off|on|cycle output=1|2"

if(len(sys.argv)!=3):
    usage()
    exit(-1)
try:
    output=int(sys.argv[2])
    cmd=sys.argv[1]
except:
    usage()
    exit(-1)
if output not in [1,2] or cmd not in ["stat","on","off","cycle"]:
    usage()
    exit(-1)

ps= agilent('192.168.4.201',6)
if(cmd=="stat"):
    volt,curr=ps.read(output)
    print "Output %d: Voltage=%.3f, Current=%.3f"%(output,volt,curr)
elif(cmd=="off"):
    volt,curr=ps.read(output)
    print "Output %d: Voltage=%.3f, Current=%.3f"%(output,volt,curr)
    ps.off(output)
    volt,curr=ps.read(output)
    print "Output %d: Voltage=%.3f, Current=%.3f"%(output,volt,curr)
elif(cmd=="on"):
    volt,curr=ps.read(output)
    print "Output %d: Voltage=%.3f, Current=%.3f"%(output,volt,curr)
    ps.on(output)
    volt,curr=ps.read(output)
    print "Output %d: Voltage=%.3f, Current=%.3f"%(output,volt,curr)
elif(cmd=="cycle"):
    volt,curr=ps.read(output)
    print "Output %d: Voltage=%.3f, Current=%.3f"%(output,volt,curr)
    ps.off(output)
    volt,curr=ps.read(output)
    print "Output %d: Voltage=%.3f, Current=%.3f"%(output,volt,curr)
    time.sleep(1)
    ps.on(output)
    volt,curr=ps.read(output)
    print "Output %d: Voltage=%.3f, Current=%.3f"%(output,volt,curr)



