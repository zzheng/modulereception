## RD53A Testing

Scripts for running tests with and comparing RD53A readout systems 

### Scripts
Contains scripts to set up and run repeated scans and tunings with Yarr on RCE or a PC

### Analysis
Contains notebooks etc for analysing the results 

### Data 
Where to put your data 

## Setup

Log on to rdev111.slac.stanford.edu with your SLAC username 
```
ssh -XY username@rddev111.slac.stanford.edu
```
From /Scripts run check_readout for camera view of readout switch 
```
. check_readout.sh
```

If you need to change to the other readout system run 
```
. change_readout.sh
```

To power on the RD53A remotely run 
```
. powersupply_on.sh
```
Check that the voltage reads 1.80V and the current is ~0.5-~0.65 A

If the power supply seems to not be responding, try pinging it 
```
ping 192.168.4.201
```
This seems to fix the issue. 

Run your scans via sshing into the ZCU102 or the YarrPC 

When you are finished run: 
```
. powersupply_off.sh
```
