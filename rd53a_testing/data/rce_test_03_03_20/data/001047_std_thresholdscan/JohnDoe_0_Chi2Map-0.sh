gnuplot > ./data/001047_std_thresholdscan/JohnDoe_0_Chi2Map-0.png
set terminal png size 1280, 1024
set palette negative defined ( 0 '#D53E4F', 1 '#F46D43', 2 '#FDAE61', 3 '#FEE08B', 4 '#E6F598', 5 '#ABDDA4', 6 '#66C2A5', 7 '#3288BD')
unset key
set title "Chi2Map-0"
set xlabel "Column"
set ylabel "Row"
set cblabel "Chi2"
set xrange[0.500000:400.500000]
set yrange[0.500000:192.500000]
plot "/tmp/root/tmp_yarr_histo2d_JohnDoe_0_Chi2Map-0.dat" matrix u (($1)*((400.500000-0.500000)/400.0)+1.000000):(($2)*((192.500000-0.500000)/192.0)+1.000000):3 with image
