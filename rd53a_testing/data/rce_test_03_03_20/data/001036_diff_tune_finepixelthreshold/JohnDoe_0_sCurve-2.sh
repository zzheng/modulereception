gnuplot > ./data/001036_diff_tune_finepixelthreshold/JohnDoe_0_sCurve-2.png
set terminal png size 1280, 1024
set palette negative defined ( 0 '#D53E4F', 1 '#F46D43', 2 '#FDAE61', 3 '#FEE08B', 4 '#E6F598', 5 '#ABDDA4', 6 '#66C2A5', 7 '#3288BD')
unset key
set title "sCurve-2"
set xlabel "Vcal"
set ylabel "Occupancy"
set cblabel "Number of pixels"
set xrange[47.500000:152.500000]
set yrange[0.500000:49.500000]
plot "/tmp/root/tmp_yarr_histo2d_JohnDoe_0_sCurve-2.dat" matrix u (($1)*((152.500000-47.500000)/21.0)+50.000000):(($2)*((49.500000-0.500000)/49.0)+1.000000):3 with image
