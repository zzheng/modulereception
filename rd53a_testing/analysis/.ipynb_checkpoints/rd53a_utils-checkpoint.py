import json 
import os
import glob
import numpy as np


import itertools
import matplotlib
import matplotlib.pyplot as plt
matplotlib.rcParams['figure.dpi']= 150

import seaborn as sns 
sns.set_theme()
sns.set_style(style = "ticks")
sns.set_color_codes("colorblind")

import csv
matplotlib.rcParams['font.size'] = 8


def get_scan_times(path):

    '''returns analysis, config, processing and scan times'''
    file = 'scanLog.json'
    json_file= path+file
    #print(json_file)
    with open(json_file) as f:
        my_json = json.load(f) # Open json
        #print(my_json.keys())
        #print(my_json['timestamp'])
    return my_json['stopwatch']['analysis'],my_json['stopwatch']['config'],my_json['stopwatch']['processing'],my_json['stopwatch']['scan']
    #return int(my_json['timestamp'][-2:]),my_json['stopwatch']['config'],my_json['stopwatch']['processing'],my_json['stopwatch']['scan']

def get_scans(path,digital_start,digital_stop,analog_start,analog_stop,tot_start,tot_stop):
    '''Uses range of run numbers to get a list of scans'''
    digital_scans = []
    analog_scans = []
    tot_scans = []
    for filename in os.listdir(path):
        if "scan" in filename:
            #print(filename)
            run_number = filename.split("_")[0]
            if digital_start <= int(run_number) <= digital_stop:
                digital_scans += [path+filename+"/"]
            if analog_start <= int(run_number) <= analog_stop:
                analog_scans += [path+filename+"/"]
            if tot_start <= int(run_number) <= tot_stop:
                tot_scans += [path+filename+"/"]
    return sorted(digital_scans), sorted(analog_scans), sorted(tot_scans)


def get_scans2(date,start,stop):
    '''Uses range of run numbers to get a list of scans'''
    scans = []
    path = '../data/'+date+"/"
    for filename in os.listdir(path):
ID            #print(filename)
            run_number = filename.split("_")[0]
            if start <= int(run_number) <= stop:
                scans += [path+filename+"/"]
    return sorted(scans)


def print_times(scans):
    '''Prints information of the times in various parts of each scan '''
    a_list, c_list, p_list, s_list,total_list = [],[],[],[],[]
    for scan in scans:
        a,c,p,s = get_scan_times(scan)
        total_scan_time = a+c+p+s
        a_list += [a]
        c_list += [c]
        p_list += [p]
        s_list += [s]
        total_list += [total_scan_time]
        
    print("Analysis")
    print(a_list)
    print("Configure")
    print(c_list)
    print("Process")
    print(p_list)
    print("Scan")
    print(s_list)
    print("Mean total scan time: "+str("{:.1f}".format(np.mean(total_list))))
    print("Std dev total scan time: "+str("{:.1f}".format(np.std(total_list))))


def plot_occupancy_map(data,title="Occupancy Map", cbar_title="Hits",new_fig=True):
    if new_fig:
        plt.figure(dpi=200)
    if "Mean" in cbar_title and "Analog" in title:
        vmin = 0
        vmax = 600
    if "Mean" in cbar_title and "Digital" in title:
        vmin = 90
        vmax = 110
    elif "Standard" in cbar_title  and "Analog" in title:
        vmin = 0
        vmax = 80
    elif "Mean $\chi^2$" in cbar_title  and "Threshold Scan" in title:
        vmin = 0
        vmax = 5
    else:
        vmin = None
        vmax = None
    # Choose colormap
    cmap = plt.cm.Spectral_r

    # Get the colormap colors
    my_cmap = cmap(np.arange(cmap.N))
    
    
    
    # Define the alphas in the range from 0 to 1
    alphas = np.ones(cmap.N)*0.85 #np.linspace(0, 1, cmap.N)
    # Define the background as white
    BG = np.asarray([1., 1., 1.,])
    # Mix the colors with the background
    for i in range(cmap.N):
        my_cmap[i,:-1] = my_cmap[i,:-1] * alphas[i] + BG * (1.-alphas[i])

    # Create new colormap
    my_cmap = matplotlib.colors.ListedColormap(my_cmap)

    ax = sns.heatmap(np.array(data).T,
                 xticklabels=50,
                 yticklabels=20, cmap=my_cmap,
                 vmin=vmin, vmax=vmax,
                 cbar_kws={'label': cbar_title})

    ax.invert_yaxis()
    ax.set_yticklabels(ax.get_yticklabels(), rotation=0)
    plt.title(title)
    plt.xlabel("Column")
    plt.ylabel("Row")
    
    return 

def plot_classified_occupancy_map(data,title="Occupancy Map", cbar_title="",new_fig=True):
    if new_fig:
        plt.figure(dpi=200)
    if "Mean" in cbar_title and "Analog" in title:
        vmin = 0
        vmax = 600
    elif "Standard" in cbar_title  and "Analog" in title:
        vmin = 0
        vmax = 80
    else:
        vmin = None
        vmax = None
    my_cmap_2 = matplotlib.colors.ListedColormap(['r','b','g','y'])#['green','red','black','yellow'])
    bounds=[-1, 0.0001, 99.999, 100.001, 1000.0]
 
    norm = matplotlib.colors.BoundaryNorm(bounds, 4)
    ax = sns.heatmap(np.array(data).T,
                 xticklabels=50,
                 yticklabels=20, cmap=my_cmap_2,
                 #vmin=-1, vmax=1000,
                 norm=norm,
                 cbar_kws={'label': cbar_title+' Hits'})
    
    cbar = ax.collections[0].colorbar
    cbar.set_ticks([-.5, 50, 100, 500])
    #cbar.set_ticklabels(['0 Hits', '< 100 Hits', '100 Hits', ' > 100 Hits'])
    cbar.set_ticklabels(['Dead pixel, 0 Hits', 'Quiet, < 100 Hits', 'Perfect Response, 100 Hits', 'Noisy, > 100 Hits'])

    ax.invert_yaxis()
    ax.set_yticklabels(ax.get_yticklabels(), rotation=0)
    plt.title(title)
    plt.xlabel("Column")
    plt.ylabel("Row")
    
    return 


def plot_classified_delta_occupancy_map(data,title="Occupancy Map", cbar_title="",new_fig=True):
    if new_fig:
        plt.figure(dpi=200)
    if "Mean" in cbar_title and "Analog" in title:
        vmin = 0
        vmax = 600
    elif "Standard" in cbar_title  and "Analog" in title:
        vmin = 0
        vmax = 80
    else:
        vmin = None
        vmax = None
    my_cmap_2 = matplotlib.colors.ListedColormap(['b','g','y'])#['green','red','black','yellow'])
    bounds=[-1000, -0.0001, 0.0001, 1000.0]
 
    norm = matplotlib.colors.BoundaryNorm(bounds, 3)
    ax = sns.heatmap(np.array(data).T,
                 xticklabels=50,
                 yticklabels=20, cmap=my_cmap_2,
                 #vmin=-1, vmax=1000,
                 norm=norm,
                 cbar_kws={'label': cbar_title+' Hits'})
    print(np.array(data).T[0:10,0:10])
    cbar = ax.collections[0].colorbar
    cbar.set_ticks([-500, 0, 500])
    #cbar.set_ticklabels(['0 Hits', '< 100 Hits', '100 Hits', ' > 100 Hits'])
    cbar.set_ticklabels(['YARR < RCE', 'YARR == RCE', 'YARR > RCE'])

    ax.invert_yaxis()
    ax.set_yticklabels(ax.get_yticklabels(), rotation=0)
    plt.title(title)
    plt.xlabel("Column")
    plt.ylabel("Row")
    
    return 

def plot_binary_occupancy_map(data,title="Occupancy Map", cbar_titles=[ 'YARR !=RCE','YARR == RCE'],new_fig=True):
    if new_fig:
        plt.figure(dpi=200)

    my_cmap_2 = matplotlib.colors.ListedColormap(['y','g'])#['green','red','black','yellow'])
    bounds=[-0.0001, 0.0001, 1.0]
    
    norm = matplotlib.colors.BoundaryNorm(bounds, 3)
    ax = sns.heatmap(np.array(data).T,
                 xticklabels=50,
                 yticklabels=20, cmap=my_cmap_2,
                 #vmin=-1, vmax=1000,
                 norm=norm,
                 cbar_kws={'label': ""})
    
    cbar = ax.collections[0].colorbar
    cbar.set_ticks([ 0, 0.5])
    #cbar.set_ticklabels(['0 Hits', '< 100 Hits', '100 Hits', ' > 100 Hits'])
    cbar.set_ticklabels(cbar_titles)

    ax.invert_yaxis()
    ax.set_yticklabels(ax.get_yticklabels(), rotation=0)
    plt.title(title)
    plt.xlabel("Column")
    plt.ylabel("Row")
    
    return 

def plot_hits_1d(data,label,color="None",title=""):
    nbins = 50
    my_range = (0,600)
    plt.hist(data,range=my_range,bins=nbins,alpha=0.8,label=label,color=color)
    plt.yscale('log')
    plt.ylabel("Number of Pixels")
    plt.xlabel("Number of Hits")
    plt.legend(frameon=False)
    plt.title(title)
    return

def plot_hists_by_fe(my_data, readout, scan_name,n_repeat,title):
    palette = itertools.cycle(sns.color_palette())
    # number of columns per type: synch = 128, lin = 136, diff = 136
    #plt.figure()
    plt.figure(figsize=(12, 3), dpi=300)
    plt.subplot(1,3,3)
    plot_hits_1d(my_data[264:400,:,0:n_repeat].flatten(),"Diff",color=next(palette),title=title)
    plt.subplot(1,3,1)
    plot_hits_1d(my_data[0:128,:,0:n_repeat].flatten(),label="Synch",color=next(palette),title=title)
    plt.subplot(1,3,2)
    plot_hits_1d(my_data[128:264,:,0:n_repeat].flatten(),label="Lin",color=next(palette),title=title)
    plt.tight_layout()
    plt.subplots_adjust(hspace=0.5, wspace=0.3)
    return

def plot_tot_1d(data,label,color="None",title=""):
    nbins = 15
    my_range = (0,15)
    plt.hist(data,range=my_range,bins=nbins,alpha=0.8,label=label,color=color,density=True)
    #plt.yscale('log')
    plt.ylabel("Normalized Number of Pixels")

    plt.xlabel("Mean ToT")
    plt.legend(frameon=False)
    plt.title(title)
    return

def plot_tot_sigma_1d(data,label,color="None",title=""):
    nbins = 10
    my_range = (0,1.0)
    plt.hist(data,range=my_range,bins=nbins,alpha=0.8,label=label,color=color,density=True)
    #plt.yscale('log')
    plt.ylabel("Normalized Number of Pixels")
    plt.xlabel("Sigma ToT")
    plt.legend(frameon=False)
    plt.title(title)
    return

def plot_threshold_1d(data,label,color="None",title=""):
    nbins = 100
    my_range = (0,2000)#np.max(data))
    plt.hist(data,range=my_range,bins=nbins,alpha=0.8,label=label,color=color,density=True)
    #plt.yscale('log')
    plt.axvline(1000, color='k', alpha=0.5, linestyle='dashed', linewidth=1)
    plt.ylabel("Normalized Number of Pixels")

    plt.xlabel("Mean Threshold")
    plt.legend(frameon=False)
    plt.title(title)
    return

def plot_noise_1d(data,label,color="None",title=""):
    nbins = 100
    my_range = (0,250)#np.max(data))
    plt.hist(data,range=my_range,bins=nbins,alpha=0.8,label=label,color=color,density=True)
    #plt.yscale('log')
    #plt.axvline(1000, color='k', alpha=0.5, linestyle='dashed', linewidth=1)
    plt.ylabel("Normalized Number of Pixels")

    plt.xlabel("Mean Noise")
    plt.legend(frameon=False)
    plt.title(title)
    return

def plot_chi2_1d(data,label,color="None",title=""):
    nbins = 50
    my_range = (0,5)#np.max(data))
    plt.hist(data,range=my_range,bins=nbins,alpha=0.8,label=label,color=color,density=True)
    #plt.yscale('log')
    #plt.axvline(1000, color='k', alpha=0.5, linestyle='dashed', linewidth=1)
    plt.ylabel("Normalized Number of Pixels")

    plt.xlabel(r"Mean $\chi^2$/ndf")
    plt.legend(frameon=False)
    plt.title(title)
    return

def plot_tot_by_fe(my_data_mean, my_data_sigma, readout, scan_name,n_repeat,title):
    palette = itertools.cycle(sns.color_palette())
    # number of columns per type: synch = 128, lin = 136, diff = 136
    #plt.figure()
    plt.figure(figsize=(12, 3), dpi=300)
    plt.subplot(1,3,3)
    plot_tot_1d(my_data_mean[264:400,:,0:n_repeat].flatten(),"Diff",color=next(palette),title=title)
    plt.subplot(1,3,1)
    plot_tot_1d(my_data_mean[0:128,:,0:n_repeat].flatten(),label="Synch",color=next(palette),title=title)
    plt.subplot(1,3,2)
    plot_tot_1d(my_data_mean[128:264,:,0:n_repeat].flatten(),label="Lin",color=next(palette),title=title)
    plt.tight_layout()
    plt.subplots_adjust(hspace=0.5, wspace=0.3)

    palette = itertools.cycle(sns.color_palette())

    plt.figure(figsize=(12, 3), dpi=300)
    plt.subplot(1,3,3)
    plot_tot_sigma_1d(my_data_sigma[264:400,:,0:n_repeat].flatten(),"Diff",color=next(palette),title=title)
    plt.subplot(1,3,1)
    plot_tot_sigma_1d(my_data_sigma[0:128,:,0:n_repeat].flatten(),label="Synch",color=next(palette),title=title)
    plt.subplot(1,3,2)
    plot_tot_sigma_1d(my_data_sigma[128:264,:,0:n_repeat].flatten(),label="Lin",color=next(palette),title=title)
    plt.tight_layout()
    plt.subplots_adjust(hspace=0.5, wspace=0.3)

    return

def plot_thresholds_by_fe(my_data_mean, readout, scan_name,n_repeat,title):
    palette = itertools.cycle(sns.color_palette())
    # number of columns per type: synch = 128, lin = 136, diff = 136
    #plt.figure()
    plt.figure(figsize=(12, 3), dpi=300)
    plt.subplot(1,3,3)
    plot_threshold_1d(my_data_mean[264:400,:,0:n_repeat].flatten(),"Diff",color=next(palette),title=title)
    plt.subplot(1,3,1)
    plot_threshold_1d(my_data_mean[0:128,:,0:n_repeat].flatten(),label="Synch",color=next(palette),title=title)
    plt.subplot(1,3,2)
    plot_threshold_1d(my_data_mean[128:264,:,0:n_repeat].flatten(),label="Lin",color=next(palette),title=title)
    plt.tight_layout()
    plt.subplots_adjust(hspace=0.5, wspace=0.3)


def plot_noise_by_fe(my_data_mean, readout, scan_name,n_repeat,title):
    palette = itertools.cycle(sns.color_palette())
    # number of columns per type: synch = 128, lin = 136, diff = 136
    #plt.figure()
    plt.figure(figsize=(12, 3), dpi=300)
    plt.subplot(1,3,3)
    plot_noise_1d(my_data_mean[264:400,:,0:n_repeat].flatten(),"Diff",color=next(palette),title=title)
    plt.subplot(1,3,1)
    plot_noise_1d(my_data_mean[0:128,:,0:n_repeat].flatten(),label="Synch",color=next(palette),title=title)
    plt.subplot(1,3,2)
    plot_noise_1d(my_data_mean[128:264,:,0:n_repeat].flatten(),label="Lin",color=next(palette),title=title)
    plt.tight_layout()
    plt.subplots_adjust(hspace=0.5, wspace=0.3)


def plot_chi2_by_fe(my_data_mean, readout, scan_name,n_repeat,title):
    palette = itertools.cycle(sns.color_palette())
    # number of columns per type: synch = 128, lin = 136, diff = 136
    #plt.figure()
    plt.figure(figsize=(12, 3), dpi=300)
    plt.subplot(1,3,3)
    plot_chi2_1d(my_data_mean[264:400,:,0:n_repeat].flatten(),"Diff",color=next(palette),title=title)
    plt.subplot(1,3,1)
    plot_chi2_1d(my_data_mean[0:128,:,0:n_repeat].flatten(),label="Synch",color=next(palette),title=title)
    plt.subplot(1,3,2)
    plot_chi2_1d(my_data_mean[128:264,:,0:n_repeat].flatten(),label="Lin",color=next(palette),title=title)
    plt.tight_layout()
    plt.subplots_adjust(hspace=0.5, wspace=0.3)

def get_formatted_data(my_scans,file):
    x, y = 400,192
    z = 100
    my_data = np.zeros((x,y,z))
    i=0
    for my_file in sorted(my_scans): 
        #print(my_file)
        try:
            with open(my_file+file,'r') as f:
                #my_json = json.load(f) # Open json
                my_json = json.loads(f.read()) # Open json
                my_data[:,:,i] = np.array(my_json['Data'])
                #plot_occupancy_map(my_json["Data"])

            i += 1
        except:
            print(i)
    my_data = my_data[:,:,0:i]
    return my_data

def get_save_and_scan_names(my_scans):
    readout = my_scans[0].split("/")[2].split('_')[0]
    scan_type = my_scans[0].split("/")[3].split("_")[-1]
    return readout, scan_type

title_dict = {
    "yarr":"YARR",
    "rce": "RCE",
    "digitalscan":"Digital Scan",
    "analogscan":"Analog Scan",
    "totscan":"Time over Threshold Scan",
    "thresholdscan":"Threshold Scan"
}

def create_occupancy_map_summary_plots(my_scans,file_name = 'JohnDoe_0_OccupancyMap.json'):

    my_data = get_formatted_data(my_scans, file_name)

    # Get titles 
    readout, scan_name = get_save_and_scan_names(my_scans)
    save_name = readout+scan_name
    n_trials = my_data.shape[2]
    if n_trials == 1: 
        n_trials_string = "Single Trial"
    else:
        n_trials_string = str(n_trials)+" Trials"
    title = title_dict[readout]+", "+title_dict[scan_name]+", "+n_trials_string    

    # Plot single 
    plot_occupancy_map(my_data[:,:,0:1].reshape(400,192),title= title_dict[readout]+", "+title_dict[scan_name]+", Single Trial",cbar_title="") 
    #plt.savefig("plots/"+save_name+"_mean_occ.png")
    print(my_data.shape)
    # Plot average
    avg_data = np.mean(my_data,axis=2)
    plot_occupancy_map(avg_data,title= title,cbar_title="Mean") 
    plt.savefig("plots/"+save_name+"_mean_occ.png")
    
    # Plot std deviation 
    std_data = np.std(my_data,axis=2)
    plot_occupancy_map(std_data,title = title,cbar_title="Standard Deviation") 
    plt.savefig("plots/"+save_name+"_std_occ.png")
    
    # Plot segmented 
    plot_classified_occupancy_map(avg_data,title= title,cbar_title="Mean") 
    plt.savefig("plots/"+save_name+"_mean_segmented_occ.png")
    
    #plot_hists_by_fe(my_data, readout, scan_name,n_repeat=1)
    #plt.savefig("plots/"+save_name+"_1_hists.pdf")
    
    plot_hists_by_fe(my_data, readout, scan_name,n_repeat=100,title=title)
    plt.savefig("plots/"+save_name+"_100_hists.pdf")

    return

def create_tot_summary_plots(my_scans):

    my_data_mean = get_formatted_data(my_scans, 'JohnDoe_0_MeanToTMap-0.json')
    my_data_sigma = get_formatted_data(my_scans, 'JohnDoe_0_SigmaToTMap-0.json')
    # Get titles 
    readout, scan_name = get_save_and_scan_names(my_scans)
    save_name = readout+scan_name
    n_trials = my_data_mean.shape[2]
    
    if n_trials == 1: 
        n_trials_string = "Single Trial"
    else:
        n_trials_string = str(n_trials)+" Trials"
    title = title_dict[readout]+", "+title_dict[scan_name]+", "+n_trials_string 

    plot_tot_by_fe(my_data_mean, my_data_sigma, readout, scan_name,n_repeat=n_trials,title=title)
    
    
def create_threshold_scan_summary_plots(my_scans, chipID):

    my_data = get_formatted_data(my_scans,chipID+"_ThresholdMap-0.json")

    # Get titles 
    readout, scan_name = get_save_and_scan_names(my_scans)
    save_name = readout+scan_name+chipID
    n_trials = my_data.shape[2]
    if n_trials == 1: 
        n_trials_string = "Single Trial"
    else:
        n_trials_string = str(n_trials)+" Trials"
    title = title_dict[readout]+", "+title_dict[scan_name]+", "+n_trials_string    

    # Plot single 
    #plot_occupancy_map(my_data[:,:,0:1].reshape(400,192),title= title_dict[readout]+", "+title_dict[scan_name]+", Single Trial",cbar_title="Threshold") 
    #plt.savefig("plots/"+save_name+"_mean_occ.png")
    #print(my_data.shape)
    # Plot average
    avg_data = np.mean(my_data,axis=2)
    plot_occupancy_map(avg_data,title= title,cbar_title="Mean Threshold") 
    plt.savefig("plots/"+save_name+"_mean_occ.png")
    
    # # Plot std deviation 
    # std_data = np.std(my_data,axis=2)
    # plot_occupancy_map(std_data,title = title,cbar_title="Standard Deviation") 
    # plt.savefig("plots/"+save_name+"_std_occ.png")
    
    # Plot segmented
    # plot_classified_occupancy_map(avg_data,title= title,cbar_title="Mean") 
    # plt.savefig("plots/"+save_name+"_mean_segmented_occ.png")
    
    #plot_hists_by_fe(my_data, readout, scan_name,n_repeat=1)
    #plt.savefig("plots/"+save_name+"_1_hists.pdf")
    
    plot_thresholds_by_fe(my_data, readout, scan_name,n_repeat=n_trials,title=title)
    plt.savefig("plots/"+save_name+"_100_hists.pdf")

    my_data = get_formatted_data(my_scans,chipID+'_NoiseMap-0.json')

    avg_data = np.mean(my_data,axis=2)
    plot_occupancy_map(avg_data,title= title,cbar_title="Mean Noise") 
    plt.savefig("plots/"+save_name+"_mean_occ.png")

    plot_noise_by_fe(my_data, readout, scan_name,n_repeat=n_trials,title=title)
    plt.savefig("plots/"+save_name+"_100_hists.pdf")

    my_data = get_formatted_data(my_scans,chipID+'_Chi2Map-0.json')

    avg_data = np.mean(my_data,axis=2)
    plot_occupancy_map(avg_data,title= title,cbar_title="Mean $\chi^2$/ndf") 
    plt.savefig("plots/"+save_name+"_mean_occ.png")

    plot_chi2_by_fe(my_data, readout, scan_name,n_repeat=n_trials,title=title)
    plt.savefig("plots/"+save_name+"_100_hists.pdf")
    return




def compare_threshold_scan(my_scans1, my_scans2, chipID):
    my_data1 = get_formatted_data(my_scans1,chipID+"_ThresholdMap-0.json")
    my_data2 = get_formatted_data(my_scans2,chipID+"_ThresholdMap-0.json")
    avg_data1 = np.mean(my_data1,axis=2)
    avg_data2 = np.mean(my_data2,axis=2)
    title = 'before_afterglue_'+chipID
    plot_occupancy_map(avg_data1-avg_data2,title= title,cbar_title="Threshold Difference") 
    plt.savefig("plots/threholdplot_mean_occ_"+chipID+".png")

def compare_noise_scan(my_scans1, my_scans2, chipID):
    my_data1 = get_formatted_data(my_scans1,chipID+"_NoiseMap-0.json")
    my_data2 = get_formatted_data(my_scans2,chipID+"_NoiseMap-0.json")
    avg_data1 = np.mean(my_data1,axis=2)
    avg_data2 = np.mean(my_data2,axis=2)
    title = 'before_afterglue_'+chipID
    plot_occupancy_map(avg_data1-avg_data2,title= title,cbar_title="Noise Difference") 
    plt.savefig("plots/threholdplot_mean_noise_"+chipID+".png")    

    
def plot_digital_scan(my_scans1, chipID):
    my_data1 = get_formatted_data(my_scans1,chipID+"_OccupancyMap.json")
    avg_data1 = np.mean(my_data1,axis=2)
    title = chipID
    plot_occupancy_map(avg_data1,title= title,cbar_title="Occupancy") 
    plt.savefig("plots/digital_"+chipID+".png")
    
def compare_digital_scan(my_scans1, my_scans2, chipID):
    my_data1 = get_formatted_data(my_scans1,chipID+"_OccupancyMap.json")
    my_data2 = get_formatted_data(my_scans2,chipID+"_OccupancyMap.json")
    avg_data1 = np.mean(my_data1,axis=2)
    avg_data2 = np.mean(my_data2,axis=2)
    title = 'before_afterglue_'+chipID
    plot_occupancy_map(avg_data1-avg_data2,title= title,cbar_title="Occupancy Difference") 
    plt.savefig("plots/threholdplot_digital_"+chipID+".png")

def compare_analog_scan(my_scans1, my_scans2, chipID):
    my_data1 = get_formatted_data(my_scans1,chipID+"_OccupancyMap.json")
    my_data2 = get_formatted_data(my_scans2,chipID+"_OccupancyMap.json")
    avg_data1 = np.mean(my_data1,axis=2)
    avg_data2 = np.mean(my_data2,axis=2)
    title = 'before_afterglue_'+chipID
    plot_occupancy_map(avg_data1-avg_data2,title= title,cbar_title="Occupancy Difference") 
    plt.savefig("plots/threholdplot_analog_"+chipID+".png")
    
def compare_bump_scan(my_scans1, my_scans2, chipID):
    my_data1 = get_formatted_data(my_scans1,chipID+"_OccupancyMap.json")
    my_data2 = get_formatted_data(my_scans2,chipID+"_OccupancyMap.json")
    avg_data1 = np.mean(my_data1,axis=2)
    avg_data2 = np.mean(my_data2,axis=2)
    title = 'before_afterglue_'+chipID
    plot_occupancy_map(avg_data1-avg_data2,title= title,cbar_title="Occupancy Difference") 
    plt.savefig("plots/threholdplot_bumpscan_"+chipID+".png")      
    
def compare_occupancy_map_summary_plots(rce_scans, yarr_scans, chipID):

    rce_data = get_formatted_data(rce_scans, chipID+'_OccupancyMap.json')
    yarr_data = get_formatted_data(yarr_scans, chipID+'_OccupancyMap.json')

    # Get titles 
    # readout, scan_name = get_save_and_scan_names(my_scans)
    # save_name = readout+scan_name
    # n_trials = my_data.shape[2]
    # if n_trials == 1: 
    #     n_trials_string = "Single Trial"
    # else:
    #     n_trials_string = str(n_trials)+" Trials"
    # title = title_dict[readout]+", "+title_dict[scan_name]+", "+n_trials_string    

    # Plot single 
    #plot_occupancy_map(my_data[:,:,0:1].reshape(400,192),title= title_dict[readout]+", "+title_dict[scan_name]+", Single Trial",cbar_title="") 
    #plt.savefig("plots/"+save_name+"_mean_occ.png")
    #print(my_data.shape)
    # Plot average
    noise_cutoff  = 100
    rce_mean_data = np.mean(rce_data,axis=2)
    rce_mean_data_good = np.where(np.logical_and(rce_mean_data<100.0001,rce_mean_data>99.999),1,0)
    rce_mean_data_dead = np.where(rce_mean_data<0.0001,0,1)
    rce_mean_data_noisy = np.where(rce_mean_data>noise_cutoff,0,1)
    title = "RCE Good Pixels, Averaged over 100 Trials"
    #plot_occupancy_map(rce_mean_data_good,title= title,cbar_title="Good (1), Bad (0)")
    plot_classified_occupancy_map(rce_mean_data,title= title,cbar_title="Mean") 

    yarr_mean_data = np.mean(yarr_data,axis=2)
    yarr_mean_data_good = np.where(np.logical_and(yarr_mean_data<100.0001,yarr_mean_data>99.999),1,0)
    yarr_mean_data_dead = np.where(yarr_mean_data<0.0001,0,1)
    yarr_mean_data_noisy = np.where(yarr_mean_data>noise_cutoff,0,1)
    title = "YARR Good Pixels, Averaged over 100 Trials"
    #plot_occupancy_map(yarr_mean_data_good,title= title,cbar_title="Good (1), Bad (0)")
    plot_classified_occupancy_map(yarr_mean_data,title= title,cbar_title="Mean") 


    delta_mean_data = yarr_mean_data-rce_mean_data
    delta_mean_data_good = np.abs(yarr_mean_data_good-rce_mean_data_good)
    delta_mean_data_dead = 1-np.abs(yarr_mean_data_dead-rce_mean_data_dead)
    delta_mean_data_noisy = 1-np.abs(yarr_mean_data_noisy-rce_mean_data_noisy)


    #title = "Pixels where YARR and RCE disagree, Averaged over 100 Trials"
    #plot_occupancy_map(delta_mean_data,title= title,cbar_title="") 
    #plot_binary_occupancy_map(delta_mean_data_good,title= title) 

    title = "Dead Pixels RCE, Averaged over 100 Trials"
    plot_binary_occupancy_map(rce_mean_data_dead,title= title,cbar_titles=["Dead, 0 Hits","Not dead"]) 
    title = "Dead Pixels YARR, Averaged over 100 Trials"
    plot_binary_occupancy_map(yarr_mean_data_dead,title= title,cbar_titles=["Dead, 0 Hits","Not dead"]) 
    title = "Difference in Dead Pixels, Averaged over 100 Trials"
    plot_binary_occupancy_map(delta_mean_data_dead,title= title,cbar_titles=["YARR != RCE","YARR == RCE"]) 
    #plot_occupancy_map(delta_mean_data_good,title= title, cbar_title="Good (1), Bad (0)")

    title = "Noisy Pixels RCE, Averaged over 100 Trials"
    plot_binary_occupancy_map(rce_mean_data_noisy,title= title,cbar_titles=["Noisy, > "+str(noise_cutoff)+" Hits","Not Noisy"]) 
    title = "Noisy Pixels YARR, Averaged over 100 Trials"
    plot_binary_occupancy_map(yarr_mean_data_noisy,title= title,cbar_titles=["Noisy, > "+str(noise_cutoff)+" Hits","Not Noisy"]) 
    title = "Difference in Noisy Pixels, Averaged over 100 Trials"
    plot_binary_occupancy_map(delta_mean_data_noisy,title= title,cbar_titles=["YARR != RCE","YARR == RCE"]) 

    plot_hists_by_fe(rce_mean_data.reshape(400,192,1), "RCE", "Analog Scan",n_repeat=1,title=title)
    plot_hists_by_fe(yarr_mean_data.reshape(400,192,1), "YARR", "Analog Scan",n_repeat=1,title=title)
    #title = "Difference YARR - RCE, Averaged over 100 Trials"
    #plot_occupancy_map(delta_mean_data_good,title= title,cbar_title="Mean YARR - Mean RCE") 
    #plt.savefig("plots/"+save_name+"_mean_occ.png")
    
    # Plot std deviation 
    #std_data = np.std(my_data,axis=2)
    #plot_occupancy_map(std_data,title = title,cbar_title="Standard Deviation") 
    #plt.savefig("plots/"+save_name+"_std_occ.png")
    
    # Plot segmented 
    #plot_classified_delta_occupancy_map(delta_mean_data,title= title,cbar_title="Mean YARR - Mean RCE") 
    #plt.savefig("plots/"+save_name+"_mean_segmented_occ.png")
    
    #plot_hists_by_fe(my_data, readout, scan_name,n_repeat=1)
    #plt.savefig("plots/"+save_name+"_1_hists.pdf")
    
    #plot_hists_by_fe(delta_mean_data, "YARR-RCE", "Analog",n_repeat=100,title=title)
    #plt.savefig("plots/"+save_name+"_100_hists.pdf")

    return
